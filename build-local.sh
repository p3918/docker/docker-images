#!/usr/bin/env bash

VERSION=$3

IMAGE=$1
FOLDER=$2
docker build --progress=plain -t celinederoland/${IMAGE}:${VERSION} ${FOLDER}
docker tag celinederoland/${IMAGE}:${VERSION} celinederoland/${IMAGE}:latest