#!/usr/bin/env bash

### sh buil-partial.sh $IMAGE $FOLDER $VERSION

### 7.1

sh build-partial.sh php7.1 php-7.1-cli 1.1
sh build-partial.sh php7.1-composer php-7.1-composer 1.1

### 7.2

sh build-partial.sh php7.2 php-7.2-cli 1.1
sh build-partial.sh php7.2-composer php-7.2-composer 1.1

### 7.3

sh build-partial.sh php7.3 php-7.3-cli 1.1
sh build-partial.sh php7.3-pcov php-7.3-pcov 1.1
sh build-partial.sh php7.3-composer php-7.3-composer 1.1

### 7.4

sh build-partial.sh php7.4 php-7.4-cli 1.1
sh build-partial.sh php7.4-pcov php-7.4-pcov 1.1
sh build-partial.sh php7.4-xdebug php-7.4-xdebug 1.1
sh build-partial.sh php7.4-composer php-7.4-composer 1.1
sh build-partial.sh php7.4-apache php-7.4-apache 1.1
sh build-partial.sh php7.4-xdebug php-7.4-xdebug 1.1

### 8.0

sh build-partial.sh php8.0 php-8.0-cli 1.3
sh build-partial.sh php8.0-web php-8.0-web-cli 1.3
sh build-partial.sh php8.0-cli-xdebug php-8.0-cli-xdebug 1.5
sh build-partial.sh php8.0-pcov php-8.0-pcov 1.3
sh build-partial.sh php8.0-composer php-8.0-composer 1.3
sh build-partial.sh php8.0-apache php-8.0-apache 1.3
sh build-partial.sh php8.0-apache-xdebug php-8.0-apache-xdebug 1.5
sh build-partial.sh php8.0-apache-blackfire php-8.0-apache-blackfire 1.0

### 8.0 + Symfony

sh build-partial.sh php8.0-sf-cli php-8.0-sf-cli 1.0
sh build-partial.sh php8.0-sf-apache php-8.0-sf-apache 1.0
sh build-partial.sh php8.0-sf-apache-xdebug php-8.0-sf-apache-xdebug 1.0

### 8.1

sh build-partial.sh php8.1-cli php-8.1-cli 1.0
sh build-partial.sh php8.1-cli-xdebug php-8.1-cli-xdebug 1.0
sh build-partial.sh php8.1-cli-pcov php-8.1-cli-pcov 1.0
sh build-partial.sh php8.1-composer php-8.1-composer 1.O
sh build-partial.sh php8.1-apache php-8.1-apache 1.0
sh build-partial.sh php8.1-apache-xdebug php-8.1-apache-xdebug 1.0
sh build-partial.sh php8.1-apache-blackfire php-8.1-apache-blackfire 1.0

### 8.1 + Symfony

sh build-partial.sh php8.1-sf-cli php-8.1-sf-cli 1.0
sh build-partial.sh php8.1-sf-apache php-8.1-sf-apache 1.0
sh build-partial.sh php8.1-sf-apache-xdebug php-8.1-sf-apache-xdebug 1.0