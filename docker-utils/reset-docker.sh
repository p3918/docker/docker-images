#!/usr/bin/env bash

sudo systemctl stop docker
sudo systemctl enable docker
sudo systemctl start docker
ps aux | grep docker