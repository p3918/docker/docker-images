#!/usr/bin/env bash
echo $(htpasswd -nb {{user}} {{password}}) | sed -e s/\\$/\\$\\$/g